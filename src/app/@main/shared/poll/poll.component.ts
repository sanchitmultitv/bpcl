import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ChatService } from 'src/app/services/chat.service';
import { DataService } from 'src/app/services/data.service';
declare var $:any;
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Component({
  selector: 'app-poll',
  templateUrl: './poll.component.html',
  styleUrls: ['./poll.component.scss']
})
export class PollComponent implements OnInit {
  width;
  progress=20
  pollingList:any =[];
  poll_id;
  showmsg=true;
  showPoll = false;
  pollForm = new FormGroup({
    polling: new FormControl(''),
  });
  msg;
  hello = [];
  audi_id;
  shown;
  lol;
  result:[]=[]
 
  constructor(private _ds : DataService,private chat : ChatService) { }
  
  ngOnInit(): void {
   
    this.chat.getSocketMessages().subscribe(((data:any)=>{
      console.log(data)
      let poll = data;
      let polls = poll.split('_');
      if (polls[0] == 'start' && polls[1]=='poll'){

         this.lol=true;
         this.shown=false;

       

          $('#poll_modal').modal('show')
     
        this.poll_id = polls[2];
        console.log(this.poll_id)
        this.audi_id = polls[3];
         this.getPollsList()  
      //  this.getpollresult();   
      }
      if (polls[0] == 'stop' && polls[1]=='poll'){
        $('#poll_modal').modal('hide')
        this.pollingList = [];

      }
      if (polls[0] == 'poll' && polls[1]=='result'){
        this.pollingList = [];
        this.getpollresult();
        $('#poll_modal').modal('show')
        // this.pollingList = [];

      }
      
    }));
  }
 
  closeModal() {
    $('#poll_modal').modal('hide');
  }
  getPollsList() {
    console.log(this.poll_id)
    this._ds.getPollsList(this.poll_id).subscribe((res:any)=>{
      this.pollingList = res.result;
    })
  }
  getpollresult(){
  console.log(this.poll_id)
    this._ds.getPollResult(this.poll_id).subscribe((res:any)=>{
      this.result=res.result.data
      // this.hello = [];
      
       // this.progress=res.result.data[1].percentage
      //  for (let i in this.result) {
      //   if (this.result[i] == 0) {
      //     this.hello.push('red');
      //   } else if (this.result[i] == 1) {
      //     this.hello.push('yellow');
      //   } else if (this.result[i] == 2) {
      //     this.hello.push('green');
      //   } else if (this.result[i] == 3) {
      //     this.hello.push('blue');
      //   } 
      // }
    //  console.log(this.progress)
    
      console.log(this.result)
    })
  }

  // sbh
  // TranscodingData() {
  //   this.fd.getTranscoding(this.token).pipe(map((data) => {
  //         this.liveList = data.result;
  //         console.log(this.liveList);
  //         this.status = data.result.map((item) => {
  //           return item.status;
  //         });
  //         this.hello = [];
  //         this.image = [];

  //         for (let i in this.status) {
  //           if (this.status[i] == 0) {
  //             this.hello.push('Inactive');
  //           } else if (this.status[i] == 1) {
  //             this.hello.push('Active');
  //           } else if (this.status[i] == 2) {
  //             this.hello.push('In-process');
  //           } else if (this.status[i] == 3) {
  //             this.hello.push('Completed');
  //           } else if (this.status[i] == 4) {
  //             this.hello.push('Failed');
  //           } else if (this.status[i] == 5) {
  //             this.hello.push('Started');
  //           } else if (this.status[i] == 6) {
  //             this.hello.push('Stopped');
  //           }
  //         }
         
  //         console.log(this.hello);
  //         console.log(this.image);
  //       })).subscribe();
  // }
  // sbg

  pollSubmit(id){
    let data = JSON.parse(localStorage.getItem('virtual'));    
    this._ds.pollSubmit(id,data.id,this.pollForm.value.polling).subscribe(res=>{
      if(res.code == 1){
        // this.getpollresult();
        this.shown=true;
        Swal.fire({
          position: 'top',
          icon: 'success',
          title: 'Thank you for submitting your answer.',
          showConfirmButton: false,
          timer: 2000
        });    
        $('#poll_modal').modal('hide');  
        //  setInterval(function() {
        // $('#poll_modal').modal('hide'); 
        // }, 2000);  
          
      }
      this.pollingList = [];
    });
  }

}

