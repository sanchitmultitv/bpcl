import { Component, Input, OnInit } from '@angular/core';
declare var $:any;

@Component({
  selector: 'app-pdfmodal',
  templateUrl: './pdfmodal.component.html',
  styleUrls: ['./pdfmodal.component.scss']
})
export class PdfmodalComponent implements OnInit {
  @Input() rName:any;
  constructor(

  ) { }

  ngOnInit(): void {
  
    $('#iframe_feedback').on("load", ()=> {
      console.log('iframe loaded'); //replace with code to hide loader
  });
  }
  closeModal(){
    $("#pdf_modal").modal('hide');
    
  }
  closeModal2(){

    $("#socratixagenda").modal('hide');
    $("#mercurixagenda").modal('hide');
    $("#rytinkagenda").modal('hide');
    $("#pdf_modal").modal('show');

  }

  openPopupSocratix(){
    $("#pdf_modal").modal('hide');
    $("#socratixagenda").modal('show');

  }

  openPopupMercurix(){
    $("#pdf_modal").modal('hide');
    $("#mercurixagenda").modal('show');

  }

  openPopupRytink(){
    $("#pdf_modal").modal('hide');
    $("#rytinkagenda").modal('show');

  }

}
