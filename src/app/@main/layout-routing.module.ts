import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PhotocontestComponent } from '../@core/photocontest/photocontest.component';
import { ComingSoonComponent } from '../coming-soon/coming-soon.component';
import { LayoutComponent } from './layout.component';
import { WallComponent } from './wall/wall.component';

const routes: Routes = [
  { path:'', redirectTo:'lobby', pathMatch:'full' },
  { path: 'lobby', loadChildren: ()=>import('../@core/lobby/lobby.module').then(m=>m.LobbyModule)},
  { path: 'auditorium', loadChildren: ()=>import('../@core/auditorium/auditorium.module').then(m=>m.AuditoriumModule) },
  { path: 'exhibition', loadChildren: ()=>import('../@core/exhibition-hall/exhibition-hall.module').then(m=>m.ExhibitionHallModule) },
  { path: 'lounge', loadChildren: ()=>import('../@core/networking-lounge/networking-lounge.module').then(m=>m.NetworkingLoungeModule) },
  { path: 'gamezone', loadChildren: ()=>import('../@core/game-zone/game-zone.module').then(m=>m.GameZoneModule) },
  { path: 'photobooth', loadChildren: ()=>import('../@core/photobooth/photobooth.module').then(m=>m.PhotoboothModule) },
  { path: 'helpdesk', loadChildren: ()=>import('../@core/helpdesk/helpdesk.module').then(m=>m.HelpdeskModule) },
  { path: 'signaturewall', loadChildren: ()=>import('../@core/signaturewall/signaturewall.module').then(m=>m.SignaturewallModule) },
  { path: 'skiplogin', component:ComingSoonComponent},
  { path: 'wall', component:WallComponent},
  { path: 'photocontest', component:PhotocontestComponent},
  { path: 'events', loadChildren: () => import('../@core/inaugural/inaugural.module').then(m => m.InauguralModule) },
  { path: 'business', loadChildren: () => import('../@core/business/business.module').then(m => m.BusinessModule) },
  { path: 'passion', loadChildren: () => import('../@core/passion/passion.module').then(m => m.PassionModule) },
  { path: 'passion-gallery', loadChildren: () => import('../@core/passion-gallery/passion-gallery.module').then(m => m.PassionGalleryModule) },
  { path: 'bpclstories', loadChildren: () => import('../@core/bpclstories/bpclstories.module').then(m => m.BpclstoriesModule) },
  { path: 'cheeritout', loadChildren: () => import('../@core/cheeritout/cheeritout.module').then(m => m.CheeritoutModule) },
  { path: 'litwit', loadChildren: () => import('../@core/litwit/litwit.module').then(m => m.LitwitModule) },
  { path: 'archiveipassion', loadChildren: () => import('../@core/archiveipassion/archiveipassion.module').then(m => m.ArchiveipassionModule) },
  { path: 'archivemercurix', loadChildren: () => import('../@core/archivemercurix/archivemercurix.module').then(m => m.ArchivemercurixModule) },
  { path: 'archivesocratix', loadChildren: () => import('../@core/archivesoctratix/archivesoctratix.module').then(m => m.ArchivesoctratixModule) },
  { path: 'archiverytink', loadChildren: () => import('../@core/archiverytink/archiverytink.module').then(m => m.ArchiverytinkModule) },
  { path: 'bpclstoryarchive', loadChildren: () => import('../@core/bpclstoryarchive/bpclstoryarchive.module').then(m => m.BpclstoryarchiveModule) },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }
