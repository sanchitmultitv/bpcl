import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-animate',
  templateUrl: './animate.component.html',
  styleUrls: ['./animate.component.scss']
})
export class AnimateComponent implements OnInit {

  constructor(private router: Router) { }
  data:any;
  ngOnInit(): void {
  }
  endVideo(){
    this.router.navigate(['/lobby']);
    this.data = JSON.parse(localStorage.getItem('virtual')).name;
    function speak2(text) {
      var utterance = new SpeechSynthesisUtterance();
      utterance.text = text;
      utterance.lang = "en-US";

      utterance.rate = 1;

      speechSynthesis.speak(utterance);
    }
    speak2('Welcome '+this.data);
  }
  skip(){
    let vid: any = document.getElementById('loginVideo');
    vid.pause();
    vid.currentActiveTime = 0;
    this.router.navigate(['/lobby']);
    this.data = JSON.parse(localStorage.getItem('virtual')).name;

    function speak2(text) {
      var utterance = new SpeechSynthesisUtterance();
      utterance.text = text;
      utterance.lang = "en-US";

      utterance.rate = 1;

      speechSynthesis.speak(utterance);
    }
    speak2('Welcome '+this.data);
  }

}
