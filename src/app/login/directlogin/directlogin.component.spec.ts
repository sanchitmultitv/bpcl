import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DirectloginComponent } from './directlogin.component';

describe('DirectloginComponent', () => {
  let component: DirectloginComponent;
  let fixture: ComponentFixture<DirectloginComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DirectloginComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DirectloginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
