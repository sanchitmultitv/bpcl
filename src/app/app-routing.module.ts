import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './@main/layout.component';
import { AnimateComponent } from './login/animate/animate.component';
import { DirectloginComponent } from './login/directlogin/directlogin.component';
import { MaincallComponent } from './maincall/maincall.component';

const routes: Routes = [
  { path:'', redirectTo:'login', pathMatch:'prefix'},

  { path:'signup', loadChildren: () => import('./signup/signup.module').then(m => m.SignupModule) },
  { path: 'login', loadChildren: () => import('./login/login.module').then(m => m.LoginModule) },
  { path: 'maincall', component:MaincallComponent},
  { path: 'animate', component:AnimateComponent},
  { path: 'directLogin', component:DirectloginComponent},
  // { path: 'directlogin', component:DirectloginComponent},
  {
    path: '',
    component: LayoutComponent,
    children: [
        { path: '', loadChildren: ()=>import('./@main/layout.module').then(m=>m.LayoutModule)}
      ],
  },
  // { path: 'archiveipassion', loadChildren: () => import('./@core/archiveipassion/archiveipassion.module').then(m => m.ArchiveipassionModule) },
  // { path: 'archivemercurix', loadChildren: () => import('./@core/archivemercurix/archivemercurix.module').then(m => m.ArchivemercurixModule) },
  // { path: 'archivesocratix', loadChildren: () => import('./@core/archivesoctratix/archivesoctratix.module').then(m => m.ArchivesoctratixModule) },
  // { path: 'archiverytink', loadChildren: () => import('./@core/archiverytink/archiverytink.module').then(m => m.ArchiverytinkModule) },
  // { path: 'litwit', loadChildren: () => import('./@core/litwit/litwit.module').then(m => m.LitwitModule) },
  // { path: 'cheeritout', loadChildren: () => import('./@core/cheeritout/cheeritout.module').then(m => m.CheeritoutModule) },
  // { path: 'bpclstories', loadChildren: () => import('./@core/bpclstories/bpclstories.module').then(m => m.BpclstoriesModule) },
  // { path: 'passion-gallery', loadChildren: () => import('./@core/passion-gallery/passion-gallery.module').then(m => m.PassionGalleryModule) },
  // { path: 'passion', loadChildren: () => import('./@core/passion/passion.module').then(m => m.PassionModule) },
  // { path: 'inaugural', loadChildren: () => import('./@core/inaugural/inaugural.module').then(m => m.InauguralModule) },
  // { path: 'business', loadChildren: () => import('./@core/business/business.module').then(m => m.BusinessModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
