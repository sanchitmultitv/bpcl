import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ArchivemercurixComponent } from './archivemercurix.component';

const routes: Routes = [{ path: '', component: ArchivemercurixComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArchivemercurixRoutingModule { }
