import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ArchivemercurixRoutingModule } from './archivemercurix-routing.module';
import { ArchivemercurixComponent } from './archivemercurix.component';


@NgModule({
  declarations: [ArchivemercurixComponent],
  imports: [
    CommonModule,
    ArchivemercurixRoutingModule
  ]
})
export class ArchivemercurixModule { }
