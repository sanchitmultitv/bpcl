import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ArchiveipassionRoutingModule } from './archiveipassion-routing.module';
import { ArchiveipassionComponent } from './archiveipassion.component';


@NgModule({
  declarations: [ArchiveipassionComponent],
  imports: [
    CommonModule,
    ArchiveipassionRoutingModule
  ]
})
export class ArchiveipassionModule { }
