import { Component, OnInit } from "@angular/core";
import { DataService } from "src/app/services/data.service";
declare var Clappr: any;
declare var $: any;

@Component({
  selector: 'app-archiveipassion',
  templateUrl: './archiveipassion.component.html',
  styleUrls: ['./archiveipassion.component.scss']
})
export class ArchiveipassionComponent implements OnInit {
  splitted: any;

  constructor(private _ds: DataService) {}
  messageList: any;
  CPlayer;
  audiObj: any = {};

  ngOnInit(): void {
    this.getArchi();
  }

  getArchi() {
    this._ds.getArchiveData("205127").subscribe((res: any) => {
      this.messageList = res.result;
      console.log(this.messageList);
    });
  }

  playStream(stream) {
    $("#playVideo").modal("show");

    // this.splitted = stream.split("https://d3ep09c8x21fmh.cloudfront.net/");

    var playerElement = document.getElementById("player-wrapper");
    this.CPlayer = new Clappr.Player({
      source: stream,
      // source: 'https://d17uqpjc0q0ra5.cloudfront.net/abr/smil:session10-p.smil/playlist.m3u8',
      // poster: poster,
      // mute: false,
      autoPlay: true,
      height: 540,
      width: '100%',
      playInline: true,
      hideMediaControl: false,
      // hideSeekBar: true,
      disableErrorScreen: true,

      // visibilityEnableIcon: false,
      // events: {

      //   onPlay: function() { this.beatAnalytics() },
      //   onPause: function() { clearInterval(this.clearInterval); },

      // }
    });

    this.CPlayer.attachTo(playerElement);
    // this.CPlayer.listenTo(this.CPlayer, onplay  )
    // this.CPlayer.listenTo(this.CPlayer, Clappr.Events.PLAYER_PAUSE, function() { })
    // this.CPlayer.core.activeContainer.on(Clappr.Events.CONTAINER_STATE_BUFFERING, function() { console.log("playing!") })

    // $('#player-wrapper > div > .media-control').css({ 'height': '1'});
  }
  closeModalVideo() {
    $("#playVideo").modal("hide");
    $("#player-wrapper").html('');
    // this.playStream('https://d3ep09c8x21fmh.cloudfront.net/');
  }
}
