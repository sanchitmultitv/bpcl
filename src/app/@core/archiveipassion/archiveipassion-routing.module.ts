import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ArchiveipassionComponent } from './archiveipassion.component';

const routes: Routes = [{ path: '', component: ArchiveipassionComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArchiveipassionRoutingModule { }
