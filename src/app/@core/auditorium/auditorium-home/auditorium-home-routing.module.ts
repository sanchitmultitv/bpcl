import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuditoriumHomeComponent } from './auditorium-home.component';

const routes: Routes = [
  {path:'', component:AuditoriumHomeComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuditoriumHomeRoutingModule { }
