import { AfterViewInit, Component, EventEmitter, OnInit, Output } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { ActivatedRoute } from '@angular/router';
import { Eventid } from '../../../shared/eventid';
declare var $: any;
import Swal from 'sweetalert2';
import { ChatService } from 'src/app/services/chat.service';
import { DomSanitizer } from '@angular/platform-browser';

declare var $: any;


@Component({
  selector: 'app-audi-menu',
  templateUrl: './audi-menu.component.html',
  styleUrls: ['./audi-menu.component.scss']
})
export class AudiMenuComponent implements OnInit, AfterViewInit {
  @Output() myOutput: EventEmitter<any> = new EventEmitter();

  imag: any;
  event_id = Eventid.event_id;
  lik;
  heart;
  clap1:any

  Leftmenu = [
    { name: 'clap', img: 'assets/icons/clap.png', audio: 'assets/mp3/applause.mp3',count:''},
    { name: 'like', img: 'assets/icons/like.png', audio: 'like',count:'' },
    { name: 'heart', img: 'assets/icons/heart.png', audio: 'heart',count:'' },
    // {name:'Hoot', img:'assets/icons/hoot.png', audio:'assets/mp3/hoot.mp3',count:''},

    // { name: 'whistle', img: 'assets/icons/whistle.png', audio: 'assets/mp3/whistle.mp3' },
    // { name: 'quiz', img: 'assets/icons/quiz.png', audio: null }
  ];
  Rightmenu = [
    // { name: 'ask the experts', img: 'assets/icons/ask.png' },
    // { name: 'poll', img: 'assets/icons/poll.png' },
    { name: 'Leave a message', img: 'assets/icons/chat-h.png' },
    // { name: 'hand raise', img: 'https://webinar.multitvsolution.com/handraise/images/raise-hand.png' }
  ];
  like: boolean;
  flag: any = 0;
  audi_id;
  pdfData: any;
  pdfData2:any;
  pdfDataSocratix:any;
  pdfDataSocratix2:any;
  constructor(private _ds: DataService, private _ar: ActivatedRoute,private chat: ChatService,private sanitiser: DomSanitizer) {

  }

  ngOnInit(): void {
    this.pdfData2 = this.sanitiser.bypassSecurityTrustResourceUrl('https://d3ep09c8x21fmh.cloudfront.net/flippdf/index.html?pdfUrl=rytink/RYTINK%20FINAL%20CASES%202021-22.pdf');
    this.pdfData = this.sanitiser.bypassSecurityTrustResourceUrl('https://d3ep09c8x21fmh.cloudfront.net/bpcldocs/nocasestudy.pdf');
    this.pdfDataSocratix2 = this.sanitiser.bypassSecurityTrustResourceUrl('https://webinar.multitvsolution.com/flippdf/index.html?pdfUrl=Socratix%20Case%20Study%20(1).pdf');
    this.pdfDataSocratix = this.sanitiser.bypassSecurityTrustResourceUrl('https://d3ep09c8x21fmh.cloudfront.net/bpcldocs/nocasestudy.pdf');
    this._ar.paramMap.subscribe(params => {
      this.audi_id = params.get('id');
    })
    this.getLikeCount2();
  }

  ngAfterViewInit() {
    this.chat.socketConnection();
    this.chat.getSocketMessages().subscribe((data: any) => {
      console.log(data);
      this.pdfData = this.sanitiser.bypassSecurityTrustResourceUrl('https://d3ep09c8x21fmh.cloudfront.net/bpcldocs/nocasestudy.pdf');
      if (data == '100') {
        this.pdfData =this.sanitiser.bypassSecurityTrustResourceUrl('https://d3ep09c8x21fmh.cloudfront.net/bpcldocs/nocasestudy.pdf');
        
      } else if(data == '99'){
        this.pdfData = this.sanitiser.bypassSecurityTrustResourceUrl('https://d3ep09c8x21fmh.cloudfront.net/flippdf/index.html?pdfUrl=rytink/Team%206%20Crude%20oil.pdf');
        
      }else if(data == '98'){
        this.pdfData = this.sanitiser.bypassSecurityTrustResourceUrl('https://d3ep09c8x21fmh.cloudfront.net/flippdf/index.html?pdfUrl=rytink/Team%2046%20The%20case%20of%20conductivity.pdf');
        
      }else if(data == '97'){
        this.pdfData = this.sanitiser.bypassSecurityTrustResourceUrl('https://d3ep09c8x21fmh.cloudfront.net/flippdf/index.html?pdfUrl=rytink/Team%2030%20Effective%20implementation.pdf');
        
      }else if(data == '96'){
        this.pdfData = this.sanitiser.bypassSecurityTrustResourceUrl('https://d3ep09c8x21fmh.cloudfront.net/flippdf/index.html?pdfUrl=rytink/Team%203%20-%20Hi%20Star.pdf');
        
      }else if(data == '95'){
        this.pdfData = this.sanitiser.bypassSecurityTrustResourceUrl('https://d3ep09c8x21fmh.cloudfront.net/flippdf/index.html?pdfUrl=rytink/Team%2017%20Commitment%20to%20arganizational%20transformational.pdf');
        
      }else if(data == '94'){
        this.pdfData = this.sanitiser.bypassSecurityTrustResourceUrl('https://d3ep09c8x21fmh.cloudfront.net/flippdf/index.html?pdfUrl=rytink/Team%2011%20Making%20of%20the%20Petrol%20bank.pdf');
        
      }else if(data == '93'){
        this.pdfData = this.sanitiser.bypassSecurityTrustResourceUrl('https://d3ep09c8x21fmh.cloudfront.net/flippdf/index.html?pdfUrl=rytink/Team%2022%20Innovative%20Into%20plane%20refuling.pdf');
        
      }else if(data == '92'){
        this.pdfData = this.sanitiser.bypassSecurityTrustResourceUrl('https://d3ep09c8x21fmh.cloudfront.net/flippdf/index.html?pdfUrl=rytink/Team%205%20%20DRA%20Injections.pdf');
        
      }else if(data == '91'){
        this.pdfData = this.sanitiser.bypassSecurityTrustResourceUrl('https://d3ep09c8x21fmh.cloudfront.net/flippdf/index.html?pdfUrl=rytink/Team%2021%20Medical%20oxygen.pdf');
        
      }else if(data == '90'){
        this.pdfData = this.sanitiser.bypassSecurityTrustResourceUrl('https://d3ep09c8x21fmh.cloudfront.net/flippdf/index.html?pdfUrl=rytink/Team%2041%20A%20story%20of%20manpower%20optimization.pdf');
        
      }else if(data == '89'){
        this.pdfData = this.sanitiser.bypassSecurityTrustResourceUrl('https://d3ep09c8x21fmh.cloudfront.net/flippdf/index.html?pdfUrl=rytink/Team%204%20-%20Overcoming%20Hostile%20conditions.pdf');
        
      }else if(data == '88'){
        this.pdfData = this.sanitiser.bypassSecurityTrustResourceUrl('https://d3ep09c8x21fmh.cloudfront.net/flippdf/index.html?pdfUrl=rytink/Team%2043%20Re%20Discovering%20Mozambique.pdf');
        
      }else if(data == '87'){
        this.pdfData = this.sanitiser.bypassSecurityTrustResourceUrl('https://d3ep09c8x21fmh.cloudfront.net/flippdf/index.html?pdfUrl=rytink/Team%2047%20BPCL%20Digital%20Transformation.pdf');
        
      }else if(data == '86'){
        this.pdfData = this.sanitiser.bypassSecurityTrustResourceUrl('https://d3ep09c8x21fmh.cloudfront.net/flippdf/index.html?pdfUrl=rytink/Team%2027%20Hydrocracker%20Incident.pdf');
        
      }else if(data == '85'){
        this.pdfData = this.sanitiser.bypassSecurityTrustResourceUrl('https://d3ep09c8x21fmh.cloudfront.net/bpcldocs/nocasestudy.pdf');
        
      }else if(data == '101'){
        this.pdfDataSocratix = this.sanitiser.bypassSecurityTrustResourceUrl('https://d3ep09c8x21fmh.cloudfront.net/bpcldocs/nocasestudy.pdf');
        
      }else if(data == '102'){
        this.pdfDataSocratix = this.sanitiser.bypassSecurityTrustResourceUrl('https://d3ep09c8x21fmh.cloudfront.net/flippdf/index.html?pdfUrl=socratix/Sequence%20-1,%20Team-11_Rohit%20Singhania.pdf');
        
      }else if(data == '103'){
        this.pdfDataSocratix = this.sanitiser.bypassSecurityTrustResourceUrl('https://d3ep09c8x21fmh.cloudfront.net/flippdf/index.html?pdfUrl=socratix/Sequence%20-2,%20Team-76_Swapnil%20Mhatre%20_%20Jasleen%20Kaur.pdf');
        
      }else if(data == '104'){
        this.pdfDataSocratix = this.sanitiser.bypassSecurityTrustResourceUrl('https://d3ep09c8x21fmh.cloudfront.net/flippdf/index.html?pdfUrl=socratix/Sequence%20-3,%20Team-106_Anjana%20Dhevi.pdf');
        
      }else if(data == '105'){
        this.pdfDataSocratix = this.sanitiser.bypassSecurityTrustResourceUrl('https://d3ep09c8x21fmh.cloudfront.net/flippdf/index.html?pdfUrl=socratix/Sequence%20-4,%20Team-205_Sachin%20Bhoria%20_%20Rituraj%20Mishra.pdf');
        
      }else if(data == '106'){
        this.pdfDataSocratix = this.sanitiser.bypassSecurityTrustResourceUrl('https://d3ep09c8x21fmh.cloudfront.net/flippdf/index.html?pdfUrl=socratix/Sequence%20-5,%20Team-94_Karthik%20S%20_%20Anjali%20R.pdf');
        
      }else if(data == '107'){
        this.pdfDataSocratix = this.sanitiser.bypassSecurityTrustResourceUrl('https://d3ep09c8x21fmh.cloudfront.net/flippdf/index.html?pdfUrl=socratix/Sequence%20-6,%20Team-63_Praveen%20Anaswara%20_%20Dilip%20Valecha.pdf');
        
      }else if(data == '108'){
        this.pdfDataSocratix = this.sanitiser.bypassSecurityTrustResourceUrl('https://d3ep09c8x21fmh.cloudfront.net/flippdf/index.html?pdfUrl=socratix/Sequence%20-7,%20Team-194_Manoj%20Jadhav.pdf');
        
      }else if(data == '109'){
        this.pdfDataSocratix = this.sanitiser.bypassSecurityTrustResourceUrl('https://d3ep09c8x21fmh.cloudfront.net/flippdf/index.html?pdfUrl=socratix/Sequence%20-8,%20Team-204_Rupam%20Dutta%20_%20Ashish%20Sharma.pdf');
        
      }else if(data == '110'){
        this.pdfDataSocratix = this.sanitiser.bypassSecurityTrustResourceUrl('https://d3ep09c8x21fmh.cloudfront.net/flippdf/index.html?pdfUrl=socratix/Sequence%20-9,%20Team-17_Visakh%20MT%20_%20Aashima%20Priye.pdf');
        
      }else if(data == '111'){
        this.pdfDataSocratix = this.sanitiser.bypassSecurityTrustResourceUrl('https://d3ep09c8x21fmh.cloudfront.net/flippdf/index.html?pdfUrl=socratix/Sequence%20-10,%20Team-81_Kumar%20Saurabh.pdf');
        
      }else if(data == '112'){
        this.pdfDataSocratix = this.sanitiser.bypassSecurityTrustResourceUrl('https://d3ep09c8x21fmh.cloudfront.net/flippdf/index.html?pdfUrl=socratix/Sequence%20-11,%20Team-120_Sameer%20Raashid%20_%20Balasubramanian%20S.pdf');
        
      }else if(data == '113'){
        this.pdfDataSocratix = this.sanitiser.bypassSecurityTrustResourceUrl('https://d3ep09c8x21fmh.cloudfront.net/flippdf/index.html?pdfUrl=socratix/Sequence%20-12,%20Team-40_Varun%20Sharma%20_%20Rudhir%20Deshkar.pdf');
        
      }else if(data == '114'){  
        this.pdfDataSocratix = this.sanitiser.bypassSecurityTrustResourceUrl('https://d3ep09c8x21fmh.cloudfront.net/bpcldocs/nocasestudy.pdf');
        
      }
    });
  }
  getLikeCount2(){
    
    this._ds.getLikeCount(this.audi_id).subscribe((data:any) => {
      // this.lik=res.result.like
      // this.heart=res.result.heart
      // this.clap1=res.result.clap
      this.Leftmenu.map((res:any)=>{
        // console.log(res)
        if(res.name==='like'){
          res.count=data.result.like
        };if(res.name==='heart'){
          res.count=data.result.heart
        };if(res.name==='clap'){
          res.count=data.result.clap
        };
        if(res.name==='Hoot'){
          res.count=data.result.Hoot
        };})
     //alert(this.lik)
     
      console.log(this.Leftmenu, 'poiuy');
    })
  }

  openRigthMenu(item) {
    if (item.includes('experts')) {
      $("#ask_question_modal").modal("show");
    }
    if (item == 'poll') {
      $("#poll_modal").modal("show");
    }
    if (item == 'Leave a message') {
      $("#t_one_groupChat").modal("show");
    }
    if (item == 'hand raise') {
      this.toggleHandRaise();
    }
  }
  /*  https://goapi.multitvsolution.com:7000/virtualapi/v1/user/comment/add
  
  POST
  
  
  event_id:182
  user_id:1234
  name:asd.mp4
  comment:like
  type:like_dislike
  to_id:155   //audi id 
  image:asd.jpg
  [5:59 pm, 28/12/2021] Divakar Multit: image = '' bhej dena */
  caseStudy(data){
    // alert("#"+data)
    
    $("#"+data).modal("show");
    // window.open('https://webinar.multitvsolution.com/flippdf/index.html?pdfUrl=Socratix%20Case%20Study%20(1).pdf','_blank');
    // https://d3ep09c8x21fmh.cloudfront.net/bpcldocs/socratix.pdf

  }
  caseStudy5(){
    // window.open('https://webinar.multitvsolution.com/flippdf/index.html?pdfUrl=Socratix%20Case%20Study%20(1).pdf','_blank');
    $("#socratixcurrent").modal("show");


  }
  caseStudy2(data){
    // alert("#"+data)
    
    $("#"+data).modal("show");
    // window.open('https://webinar.multitvsolution.com/flippdf/index.html?pdfUrl=Socratix%20Case%20Study%20(1).pdf','_blank');

  }
  caseStudy3(data){
    // alert("#"+data)
    
    $("#"+data).modal("show");
    // window.open('https://webinar.multitvsolution.com/flippdf/index.html?pdfUrl=Socratix%20Case%20Study%20(1).pdf','_blank');

  }
  caseStudy4(){
    
    $("#currentrytink").modal("show");


  }

  
  closeModal(){
    $("#socratix").modal("hide");
    $("#rytink").modal("hide");
    $("#currentrytink").modal("hide");
    $("#socratixcurrent").modal("hide");
    
  }
  closeDocs(){    
    $("#mercurix").modal("hide");

  }
  likeopen(data) {
   //  alert(data)
    if (data == 'like') {
      this.imag = 'assets/icons/like.png'
      this.like = true;
    }
    if (data == 'clap') {
      this.imag = 'assets/icons/clap.png'
      this.like = true;
    }
    if (data == 'heart') {
      this.imag = 'assets/icons/heart.png'
      this.like = true;
    }
   
    setTimeout(() => {
      this.like = false;
    }, 10000);


    // if(name=='like'){
    //   alert("ashish")
    // }
   
    const formData = new FormData();
    formData.append('event_id', this.event_id);
    formData.append('user_id', JSON.parse(localStorage.getItem('virtual')).id);
    formData.append('name', JSON.parse(localStorage.getItem('virtual')).name);
    formData.append('comment', data);
    formData.append('type', 'like_dislike');
    formData.append('audi', this.audi_id);
    formData.append('image', '');
    
   if(data=='like' || data=='clap' || data=='heart' ||data=='Hoot'){
    this._ds.postLike(formData).subscribe((res: any) => {
    if (res.code == 1) {
      this.getLikeCount2();
    console.log("post like data")
    }
    })
   
  }
 }

  toggleHandRaise() {
    Swal.fire({
      position: 'top',
      icon: 'success',
      title: 'Your request has been sent.',
      showConfirmButton: false,
      timer: 3000
    });
    if (this.flag == 0) {
      this.flag = 1;

    }
    else {
      this.flag = 0;

    }
    this.handraise();
  }

  handraise() {
    let signify_userid = JSON.parse(localStorage.getItem('getdata')).id;
    const formsData = new FormData();
    formsData.append('user_id', signify_userid);
    formsData.append('flag', this.flag);
    console.log(formsData);
    this._ds.handraise(formsData).subscribe(res => {
      console.log(res);
    })

  }

  openLeftMenu(item) {
   // alert(item)
    if (item == null) {
      $("#quiz_modal").modal("show");
     
    } else {
      var sound: any = document.createElement('audio');
      sound.id = 'audio-player';
      sound.src = item
      sound.type = 'audio/mpeg';
      sound.preload = true;
      sound.autoplay = '';
      sound.controls = 'controls';
      sound.play();
      sound.style.display = 'none';
      document.getElementById('setAudio').appendChild(sound);
    }
  }
}
