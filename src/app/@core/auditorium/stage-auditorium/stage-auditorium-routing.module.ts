import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StageAuditoriumComponent } from './stage-auditorium.component';

const routes: Routes = [
  {path:'', component:StageAuditoriumComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StageAuditoriumRoutingModule { }
