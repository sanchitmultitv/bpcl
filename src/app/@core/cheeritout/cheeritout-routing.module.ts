import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CheeritoutComponent } from './cheeritout.component';

const routes: Routes = [{ path: '', component: CheeritoutComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CheeritoutRoutingModule { }
