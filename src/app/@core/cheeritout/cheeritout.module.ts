import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CheeritoutRoutingModule } from './cheeritout-routing.module';
import { CheeritoutComponent } from './cheeritout.component';


@NgModule({
  declarations: [CheeritoutComponent],
  imports: [
    CommonModule,
    CheeritoutRoutingModule
  ]
})
export class CheeritoutModule { }
