import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
declare var $: any;

@Component({
  selector: 'app-cheeritout',
  templateUrl: './cheeritout.component.html',
  styleUrls: ['./cheeritout.component.scss']
})
export class CheeritoutComponent implements OnInit {
  allDocs: any;

  constructor(private _ds: DataService) { }

  ngOnInit(): void {
    
    this._ds.getDoc().subscribe((res) => {
      this.allDocs = res;
      console.log(this.allDocs)
      // this.router.navigate(["/login"]);
      // localStorage.clear();
    });
  }
  openDocs(){
    $("#open_popup_modal").modal("show");
  }
  closeDocs(){
    $("#open_popup_modal").modal("hide");
  }
}
