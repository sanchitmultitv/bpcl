import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InauguralComponent } from './inaugural.component';

const routes: Routes = [{ path: '', component: InauguralComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InauguralRoutingModule { }
