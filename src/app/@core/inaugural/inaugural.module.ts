import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InauguralRoutingModule } from './inaugural-routing.module';
import { InauguralComponent } from './inaugural.component';


@NgModule({
  declarations: [InauguralComponent],
  imports: [
    CommonModule,
    InauguralRoutingModule
  ]
})
export class InauguralModule { }
