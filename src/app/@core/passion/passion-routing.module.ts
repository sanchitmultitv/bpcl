import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PassionComponent } from './passion.component';

const routes: Routes = [{ path: '', component: PassionComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PassionRoutingModule { }
