import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PassionRoutingModule } from './passion-routing.module';
import { PassionComponent } from './passion.component';


@NgModule({
  declarations: [PassionComponent],
  imports: [
    CommonModule,
    PassionRoutingModule
  ]
})
export class PassionModule { }
