import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ArchiverytinkRoutingModule } from './archiverytink-routing.module';
import { ArchiverytinkComponent } from './archiverytink.component';


@NgModule({
  declarations: [ArchiverytinkComponent],
  imports: [
    CommonModule,
    ArchiverytinkRoutingModule
  ]
})
export class ArchiverytinkModule { }
