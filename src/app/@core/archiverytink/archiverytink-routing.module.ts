import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ArchiverytinkComponent } from './archiverytink.component';

const routes: Routes = [{ path: '', component: ArchiverytinkComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArchiverytinkRoutingModule { }
