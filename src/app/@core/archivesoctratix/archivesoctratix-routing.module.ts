import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ArchivesoctratixComponent } from './archivesoctratix.component';

const routes: Routes = [{ path: '', component: ArchivesoctratixComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArchivesoctratixRoutingModule { }
