import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ArchivesoctratixRoutingModule } from './archivesoctratix-routing.module';
import { ArchivesoctratixComponent } from './archivesoctratix.component';


@NgModule({
  declarations: [ArchivesoctratixComponent],
  imports: [
    CommonModule,
    ArchivesoctratixRoutingModule
  ]
})
export class ArchivesoctratixModule { }
