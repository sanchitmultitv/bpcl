import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExhibitionStallRoutingModule } from './exhibition-stall-routing.module';
import { ExhibitionStallComponent } from './exhibition-stall.component';
import { EnquiryFormComponent } from './enquiry-form/enquiry-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ExhibitorChatModule } from '../exhibitor-chat/exhibitor-chat.module';


@NgModule({
  declarations: [ExhibitionStallComponent, EnquiryFormComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ExhibitionStallRoutingModule,
    ExhibitorChatModule
  ]
})
export class ExhibitionStallModule { }
