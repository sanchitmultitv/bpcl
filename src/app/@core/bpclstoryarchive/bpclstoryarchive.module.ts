import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BpclstoryarchiveRoutingModule } from './bpclstoryarchive-routing.module';
import { BpclstoryarchiveComponent } from './bpclstoryarchive.component';


@NgModule({
  declarations: [BpclstoryarchiveComponent],
  imports: [
    CommonModule,
    BpclstoryarchiveRoutingModule
  ]
})
export class BpclstoryarchiveModule { }
