import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HelpdeskRoutingModule } from './helpdesk-routing.module';
import { HelpdeskComponent } from './helpdesk.component';
import { RouterModule } from '@angular/router';
import { PdfmodalModule } from 'src/app/@main/pdfmodal/pdfmodal.module';


@NgModule({
  declarations: [HelpdeskComponent],
  imports: [
    CommonModule,
    RouterModule,
    PdfmodalModule,
    HelpdeskRoutingModule
  ],
})
export class HelpdeskModule { }
