import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LitwitRoutingModule } from './litwit-routing.module';
import { LitwitComponent } from './litwit.component';


@NgModule({
  declarations: [LitwitComponent],
  imports: [
    CommonModule,
    LitwitRoutingModule
  ]
})
export class LitwitModule { }
