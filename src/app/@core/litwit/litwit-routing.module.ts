import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LitwitComponent } from './litwit.component';

const routes: Routes = [{ path: '', component: LitwitComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LitwitRoutingModule { }
