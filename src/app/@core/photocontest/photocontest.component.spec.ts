import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PhotocontestComponent } from './photocontest.component';

describe('PhotocontestComponent', () => {
  let component: PhotocontestComponent;
  let fixture: ComponentFixture<PhotocontestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PhotocontestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PhotocontestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
