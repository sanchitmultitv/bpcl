import { Component, OnInit } from '@angular/core';
import { DataService, localService } from 'src/app/services/data.service';
declare var $: any;

import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-photocontest',
  templateUrl: './photocontest.component.html',
  styleUrls: ['./photocontest.component.scss']
})
export class PhotocontestComponent implements OnInit {

    quizlist: any =[];
    help:any =[];
  src:any;
    constructor(private _fd: DataService,private sanitiser: DomSanitizer,) { }
  
    ngOnInit(): void { 
      this.getQuizlist();
      this.getCount();
    }

    openModal(hello){
      // alert(hello)
      this.src=this.sanitiser.bypassSecurityTrustResourceUrl(hello);
      $("#bikeRacing").modal("show");

    }
  
    // hide(){
      
    //   document.getElementById("dog").style.display = "none";
    //   document.getElementById("dog").style.display = "block";
    // }
  
    vote(path,toid) {
      // let timer: any = new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();
      // this.resetLINKS = this.router.url;
       let data = JSON.parse(localStorage.getItem('virtual'));
  
      const formData = new FormData();
       formData.append('event_id', '182');
       formData.append('user_id', data.id );
       formData.append('name', data.name );
       formData.append('comment', 'like' );
       formData.append('type', 'like_dislike' );
       formData.append('to_id', toid );
       formData.append('image', path );
  
      // let data = JSON.parse(localStorage.getItem('virtual'));
      // this.resetLINKS = this.router.url;
  
      // console.log("707007"+this.router.url);
      this._fd.postVote(formData).subscribe((res => {
        if (res.code === 1) {
          alert('Submitted Succesfully');
          this.getCount();
        }else{
          alert(res.result);
        }
      }))
  
    }
  
    getCount(){
      this._fd.getComments().subscribe((res: any) => {
        this.help = res.result;
        console.log(this.help);
        // alert(this.help)
        // this.help[0].
        // console.log('dddd', res)
      });
    }
  
    getQuizlist() {
      // let event_id = 182;
      this._fd.GetVoteData().subscribe((res: any) => {
        this.quizlist = res;
        console.log(this.quizlist);
  
        // console.log('dddd', res)
      });
    }
    
    closePopup(){
      $("#bikeRacing").modal("hide");
      
      
    }
  }
