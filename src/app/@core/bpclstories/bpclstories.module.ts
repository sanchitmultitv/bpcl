import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BpclstoriesRoutingModule } from './bpclstories-routing.module';
import { BpclstoriesComponent } from './bpclstories.component';


@NgModule({
  declarations: [BpclstoriesComponent],
  imports: [
    CommonModule,
    BpclstoriesRoutingModule
  ]
})
export class BpclstoriesModule { }
