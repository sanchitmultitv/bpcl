import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PassionGalleryRoutingModule } from './passion-gallery-routing.module';
import { PassionGalleryComponent } from './passion-gallery.component';


@NgModule({
  declarations: [PassionGalleryComponent],
  imports: [
    CommonModule,
    PassionGalleryRoutingModule
  ]
})
export class PassionGalleryModule { }
