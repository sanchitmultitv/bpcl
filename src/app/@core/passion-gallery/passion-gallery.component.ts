import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
declare var $: any;

@Component({
  selector: 'app-passion-gallery',
  templateUrl: './passion-gallery.component.html',
  styleUrls: ['./passion-gallery.component.scss']
})
export class PassionGalleryComponent implements OnInit {
  allDocs: any;

  constructor(private _ds: DataService) { }

  ngOnInit(): void {
    this._ds.getDoc().subscribe((res) => {
      this.allDocs = res;
      console.log(this.allDocs)
   
    });
  }
  openDocs(){
    $("#open_popup_modal").modal("show");
  }
  openDocs2(){
    $("#open_popup_modal2").modal("show");
  }
  closeDocs(){
    $("#open_popup_modal").modal("hide");
    $("#open_popup_modal2").modal("hide");
  }

}
