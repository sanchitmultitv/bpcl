import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PassionGalleryComponent } from './passion-gallery.component';

const routes: Routes = [{ path: '', component: PassionGalleryComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PassionGalleryRoutingModule { }
